import os
import sys
import argparse
import ast
import csv
import cv2
import json
import time
import torch
import torchvision.transforms as T

sys.path.insert(1, os.getcwd())
from SimpleHRNet import SimpleHRNet
from misc.visualization import check_video_rotation


def main(format, filename, hrnet_m, hrnet_c, hrnet_j, hrnet_weights, image_resolution, single_person, use_tiny_yolo,
         max_batch_size, csv_output_filename, csv_delimiter, json_output_filename, device):
    if device is not None:
        device = torch.device(device)
    else:
        if torch.cuda.is_available():
            torch.backends.cudnn.deterministic = True
            device = torch.device('cuda')
        else:
            device = torch.device('cpu')

    # print(device)
    model_obj = torch.load('./models/obj_detectors/scenario_5_model_compleet')
    model_obj.eval()
    image_resolution = ast.literal_eval(image_resolution)
    headers = [                 "frame_index",
				"detection_index",
				"nose_y",
				"nose_x",
				"nose_confidence",
				"left_eye_y",
				"left_eye_x",
				"left_eye_confidence",
				"right_eye_y",
				"right_eye_x",
				"right_eye_confidence",
				"left_ear_y",
				"left_ear_x",
				"left_ear_confidence",
				"right_ear_y",
				"right_ear_x",
				"right_ear_confidence",
				"left_shoulder_y",
				"left_shoulder_x",
				"left_shoulder_confidence",
				"right_shoulder_y",
				"right_shoulder_x",
				"right_shoulder_confidence",
				"left_elbow_y",
				"left_elbow_x",
				"left_elbow_confidence",
				"right_elbow_y",
				"right_elbow_x",
				"right_elbow_confidence",
				"left_wrist_y",
				"left_wrist_x",
				"left_wrist_confidence",
				"right_wrist_y",
				"right_wrist_x",
				"right_wrist_confidence",
				"left_hip_y",
				"left_hip_x",
				"left_hip_confidence",
				"right_hip_y",
				"right_hip_x",
				"right_hip_confidence",
				"left_knee_y",
				"left_knee_x",
				"left_knee_confidence",
				"right_knee_y",
				"right_knee_x",
				"right_knee_confidence",
				"left_ankle_y",
				"left_ankle_x",
				"left_ankle_confidence",
				"right_ankle_y",
				"right_ankle_x",
				"right_ankle_confidence",
				"fruit_name",
				"quality"]
    rotation_code = check_video_rotation(filename)
    video = cv2.VideoCapture(filename)
    assert video.isOpened()
    nof_frames = video.get(cv2.CAP_PROP_FRAME_COUNT)

    assert format in ('csv', 'json')
    if format == 'csv':
        assert csv_output_filename.endswith('.csv')
        fd = open(csv_output_filename, 'wt', newline='')
        csv_output = csv.writer(fd, delimiter=csv_delimiter)
        csv_output.writerow(headers)
    elif format == 'json':
        assert json_output_filename.endswith('.json')
        fd = open(json_output_filename, 'wt')
        json_data = {}

    if use_tiny_yolo:
        yolo_model_def = "./models/detectors/yolo/config/yolov3-tiny.cfg"
        yolo_class_path = "./models/detectors/yolo/data/coco.names"
        yolo_weights_path = "./models/detectors/yolo/weights/yolov3-tiny.weights"
    else:
        yolo_model_def = "./models/detectors/yolo/config/yolov3.cfg"
        yolo_class_path = "./models/detectors/yolo/data/coco.names"
        yolo_weights_path = "./models/detectors/yolo/weights/yolov3.weights"

    model = SimpleHRNet(
        hrnet_c,
        hrnet_j,
        hrnet_weights,
        model_name=hrnet_m,
        resolution=image_resolution,
        multiperson=not single_person,
        max_batch_size=max_batch_size,
        yolo_model_def=yolo_model_def,
        yolo_class_path=yolo_class_path,
        yolo_weights_path=yolo_weights_path,
        device=device
    )

    index = 0
    while True:
        t = time.time()

        ret, frame = video.read()
        if not ret:
            break
        if rotation_code is not None:
            frame = cv2.rotate(frame, rotation_code)
        # pose detection part
        pts = model.predict(frame)
        # object detection part
        transform = T.Compose([T.ToTensor()]) # Defing PyTorch Transform
        img = transform(frame) # Apply the transform to the image
        bboxes, labels = predict(img, model_obj, device, 0.5)
        # print(labels)
        dist_row = []
        # measuring euclidean distance between wrist and the found objects
        for i in range(len(bboxes)):
            centrxy = ((bboxes[i][0]+bboxes[i][2])/2,(bboxes[i][1]+bboxes[i][3])/2)
            dist = ((centrxy[0]-pts[0][10][0])**2 + (centrxy[1]-pts[0][10][0])**2)**(1/2)
            dist_row.append(dist)
        closest_to_wrist_id = dist_row.index(min(dist_row))

        # csv format is:
        #   frame_index,detection_index,<point 0>,<point 1>,...,<point hrnet_j>
        # where each <point N> corresponds to three elements:
        #   y_coordinate,x_coordinate,confidence

        # json format is:
        #   {frame_index: [[<point 0>,<point 1>,...,<point hrnet_j>], ...], ...}
        # where each <point N> corresponds to three elements:
        #   [y_coordinate,x_coordinate,confidence]

        if format == 'csv':
            for j, pt in enumerate(pts):
                row = [index, j] + pt.flatten().tolist()
                row.append(labels[closest_to_wrist_id]) # label of the detected fruit
                # row.append(min(dist_row))

                csv_output.writerow(row)
        elif format == 'json':
            json_data[index] = list()
            for j, pt in enumerate(pts):
                json_data[index].append(pt.tolist())

        fps = 1. / (time.time() - t)
        print('\rframe: % 4d / %d - framerate: %f fps ' % (index, nof_frames - 1, fps), end='')

        index += 1

    if format == 'json':
        json.dump(json_data, fd)

    fd.close()


# prediction function
def predict(image, model, device, detection_threshold):
    # scenario-5 fruits diverse
    classes_string = ['banana', 'cucumber', 'mandarin']
    # transform the image to tensor
    image = image.to(device)
    image = image.unsqueeze(0) # add a batch dimension
    outputs = model(image) # get the predictions on the image
    # get all the predicited class names
    pred_classes = outputs[0]['labels'].detach().cpu().numpy()
    # get score for all the predicted objects
    pred_scores = outputs[0]['scores'].detach().cpu().numpy()
    # get all the predicted bounding boxes
    pred_bboxes = outputs[0]['boxes'].detach().cpu().numpy()
    # get boxes and classes above the threshold score
    boxes = pred_bboxes[pred_scores >= detection_threshold].astype('int32')
    classes = pred_classes[pred_scores >= detection_threshold]
    # convert class back to string
    classes = [classes_string[i-1] for i in classes]
    
    return boxes, classes

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Extract and save keypoints in csv or json format.\n'
                    'csv format is:\n'
                    '  frame_index,detection_index,<point 0>,<point 1>,...,<point hrnet_j>\n'
                    'where each <point N> corresponds to three elements:\n'
                    '  y_coordinate,x_coordinate,confidence\n'
                    'json format is:\n'
                    '  {frame_index: [[<point 0>,<point 1>,...,<point hrnet_j>], ...], ...}\n'
                    'where each <point N> corresponds to three elements:\n'
                    '[y_coordinate,x_coordinate,confidence]')
    parser.add_argument("--format", help="output file format. CSV or JSON.",
                        type=str, default=None)
    parser.add_argument("--filename", "-f", help="open the specified video",
                        type=str, default=None)
    parser.add_argument("--hrnet_m", "-m", help="network model - HRNet or PoseResNet", type=str, default='HRNet')
    parser.add_argument("--hrnet_c", "-c", help="hrnet parameters - number of channels (if model is HRNet), "
                                                "resnet size (if model is PoseResNet)", type=int, default=48)
    parser.add_argument("--hrnet_j", "-j", help="hrnet parameters - number of joints", type=int, default=17)
    parser.add_argument("--hrnet_weights", "-w", help="hrnet parameters - path to the pretrained weights",
                        type=str, default="./weights/pose_hrnet_w48_384x288.pth")
    parser.add_argument("--image_resolution", "-r", help="image resolution", type=str, default='(384, 288)')
    parser.add_argument("--single_person",
                        help="disable the multiperson detection (YOLOv3 or an equivalen detector is required for"
                             "multiperson detection)",
                        action="store_true")
    parser.add_argument("--use_tiny_yolo",
                        help="Use YOLOv3-tiny in place of YOLOv3 (faster person detection). Ignored if --single_person",
                        action="store_true")
    parser.add_argument("--max_batch_size", help="maximum batch size used for inference", type=int, default=16)
    parser.add_argument("--csv_output_filename", help="filename of the csv that will be written.",
                        type=str, default='output.csv')
    parser.add_argument("--csv_delimiter", help="csv delimiter", type=str, default=',')
    parser.add_argument("--json_output_filename", help="filename of the json file that will be written.",
                        type=str, default='output.json')
    parser.add_argument("--device", help="device to be used (default: cuda, if available)."
                                         "Set to `cuda` to use all available GPUs (default); "
                                         "set to `cuda:IDS` to use one or more specific GPUs "
                                         "(e.g. `cuda:0` `cuda:1,2`); "
                                         "set to `cpu` to run on cpu.", type=str, default=None)
    args = parser.parse_args()
    main(**args.__dict__)
